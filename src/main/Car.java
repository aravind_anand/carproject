package main;

import java.time.LocalDate;
import java.util.Objects;

public class Car implements Comparable<Car> {

	private int slNo;
	private String company;
	private String model;
	private double price;
	private LocalDate releaseDate;
	private Colour colour;

	public Car(int slNo, String company, String model, double price, LocalDate releaseDate, Colour colour) {
		super();
		this.slNo = slNo;
		this.company = company;
		this.model = model;
		this.price = price;
		this.releaseDate = releaseDate;
		this.colour = colour;
	}

	public Car(){
		
	}
	
	
	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Colour getColour() {
		return colour;
	}

	public void setColour(Colour colour) {
		this.colour = colour;
	}

	public int getSlNo() {
		return slNo;
	}

	public void setSlNo(int slNo) {
		this.slNo = slNo;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public LocalDate getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(LocalDate releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String toString() {

		return new StringBuffer("SLNO:  ").append(slNo).append("\nCAR NAME: ").append(company)
				.append("\nMODEL NUMBER: ").append(model).append("\nPRICE OF CAR : ").append(price)
				.append("\nRELEASE DATE : ").append(releaseDate).append("\nCar colour : ").append(colour).toString();
	}

	@Override
	public int compareTo(Car car) {
		return slNo-car.slNo;
//		if (slNo < car.slNo)
//			return -1;
//		if (slNo > car.slNo)
//			return 1;
//		else
//			return 0;

	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Car other = (Car) obj;
		return colour == other.colour && Objects.equals(company, other.company) && Objects.equals(model, other.model)
				&& Double.doubleToLongBits(price) == Double.doubleToLongBits(other.price)
				&& Objects.equals(releaseDate, other.releaseDate) && slNo == other.slNo;
	}

}