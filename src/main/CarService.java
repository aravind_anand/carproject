package main;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

/*
An Interface specifies the operations on Car Object
@author aravind
*/
public interface CarService {

	/*
	 * This method will add a Car
	 * 
	 * @param car car to be added
	 */

	void add(Car car);

	/* This method will display all the car that is added */

	void displayAll();

	/*
	 * This method will search cars and display the car with the car name
	 * 
	 * @param s s is the String that we want to search
	 */

	void search(String carName);

	/*
	 * This method sorts the car based on SerailNumber
	 */
	void sort();

	/*
	 * This method will search car with a date and display the car
	 * 
	 * @param releaseDate releaseDate is the date of the car released
	 */
	List<Car> search(LocalDate releaseDate);
	/*
	 * This method will delete the car from car details
	 * 
	 * 
	 * @param delete delete is for the deletion
	 */

	void delete(String company);

	/*
	 * This method will append the data to the CVS file
	 * 
	 * 
	 * @param car car is the object of the class Car
	 */

	default String getCSVString(Car car) {

		return new StringBuffer().append(car.getSlNo()).append(",\"").append(car.getCompany()).append("\",\"")
				.append(car.getModel()).append("\",").append(car.getPrice()).append(",\"").append(car.getReleaseDate())
				.append("\",\"").append(car.getColour()).append("\"\n").toString();

	}

	/*
	 * This method will download the data in a csv format
	 */

	void downloadDetailsAsCSV() throws IOException;

	/*
	 * This method will append the data to as Json format
	 * 
	 * 
	 * @param car car is the object of the class Car
	 */

	default String getJsonString(Car car) {

		return new StringBuffer().append("{\n").append("\"SLNO\" :").append(car.getSlNo()).append(",\n")
				.append("\"Car Name\" : \"").append(car.getCompany()).append("\",\n").append("\"Model Number \": \"")
				.append(car.getModel()).append("\",\n").append("\"Car Price\" : ").append(car.getPrice()).append(",\n")
				.append("\" Release date \" : \"").append(car.getReleaseDate()).append("\",\n")
				.append("\"Car color\" : \"").append(car.getColour()).append("\"\n}").toString();

	}

	/*
	 * This method will download the data in a Json format
	 */

	void downloadDetailsAsJson() throws IOException;

}
