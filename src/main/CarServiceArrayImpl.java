package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.StringJoiner;

public class CarServiceArrayImpl implements CarService {

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	/// private Car arr[]= new Car[100];
	private int count;

	Car arr[];

	public CarServiceArrayImpl() {

		arr = new Car[100];
	}

	public CarServiceArrayImpl(int size) {

		arr = new Car[size];
	}

	/* This method will add the car */

	public void add(Car car) {
		boolean flag = true;
		if (count >= arr.length)
			System.out.println("Array is Full...!");
		else // if (flag == true)
		{
			for (int i = 0; i < count; i++) {
				if (arr[i].equals(car)) {
					System.out.println("Duplication occured ");
					flag = false;
					// break;
				}
			}

		}
		if (flag == true) {
			arr[count] = car;
			count++;
		}

	}

	/* This method will display the car details */

	public void displayAll() {
		if (count <= 0)
			System.out.println("No data");
		else {

			for (int i = 0; i < count; i++)
				System.out.println(arr[i]);
		}
	}

	/* This method will search the car */

	public void search(String carName) {
		boolean flag = false;
		for (int j = 0; j < count; j++) {
			if (arr[j].getCompany().equalsIgnoreCase(carName)) {
				System.out.println(arr[j]);
				flag = true;
			}
		}
		if (flag == false) {
			System.out.println("Search not found....!");
		}

	}

	/* This method will sort the car details */

	public void sort() {

		Arrays.sort(arr, 0, count);
		displayAll();
	}

	/* This method will search the car with specific date */

	public List<Car> search(LocalDate releaseDate) {

		List<Car>  searchCar= new ArrayList<>();

		for (int i = 0; i < count; i++) {
			if (arr[i].getReleaseDate().isAfter(releaseDate)) {
				searchCar.add(arr[i]);
//				searchCar.add((new Car(arr[i].getSlNo(),arr[i].getCompany(),arr[i].getModel(),
//						arr[i].getPrice(),arr[i].getReleaseDate(),arr[i].getColour())));
				
			}
		}

		return searchCar;
	}

	/* This method will delete the car from the car details */

	public void delete(String company) {
		int k = 0;
		for (int j = 0; j < count; j++) {
			if (!arr[j].getCompany().equals(company)) {
				arr[k] = arr[j];
				k++;
				System.out.println("Deleted");
			}
			
		}
		count=k;

		/*
		 * for (int j = 0; j < count; j++) { if (arr[j].getCompany().equals(delete)) {
		 * for (int i = j; i < count - 1; i++) { arr[i] = arr[i + 1]; } count--; j--; }
		 * } for (int i = 0; i < count; i++) { System.out.println(" " + arr[i]); }
		 */
	}

	/* This method will download the car details to the CSV file */

	public void downloadDetailsAsCSV() throws IOException {

		FileWriter fileWriter = new FileWriter("download.csv");
		for (int i = 0; i < count; i++) {
			String data = getCSVString(arr[i]);
			fileWriter.write(data);
		}
		fileWriter.close();

	}

	public void downloadDetailsAsJson() throws IOException {
		StringJoiner join = new StringJoiner(",", "[", "]");
		String data;
		FileWriter fileWriter = new FileWriter("downloadJson.json");
		for (int i = 0; i < count; i++) {

			data = getJsonString(arr[i]);
			join.add(data);
		}
		// System.out.println(join.toString());
		fileWriter.write(join.toString());

		fileWriter.close();
	}

}
