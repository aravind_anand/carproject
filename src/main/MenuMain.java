package main;

import java.util.Scanner;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.InputMismatchException;
import java.util.Properties;

public class MenuMain {

	public static void main(String[] args)throws Exception {

		Scanner sc = new Scanner(System.in);
		//CarService carService = new CarServiceArrayImpl();
		//CarService carService= new CarServiceArrayListImpl();
		FileReader read=new FileReader("src/configuration.properties");
		Properties properties=new Properties();
		properties.load(read);
//		System.out.println(properties.getProperty("user"));
//		System.out.println(properties.getProperty("password"));
		CarService carService=CarServiceFactory.getStudentservice(properties);
		int c;
		do {
			System.out.println("1. Add new Item");
			System.out.println("2. Dispaly ");
			System.out.println("3. search");
			System.out.println("4. Sort");
			System.out.println("5. Search Date");
			System.out.println("6. Delete");
			System.out.println("7. Download as CSV File");
			System.out.println("8. Download the Car details as json File");
			System.out.println("9. Exit");
			System.out.println("Enter your choice: ");
			c = sc.nextInt();
			switch (c) {
			case 1:
				try {
					System.out.println("Enter the Registration Number : ");
					int slNo = sc.nextInt();
					System.out.println("Enter the car company  : ");
					String company = sc.next();
					System.out.println("Enter the model : ");
					String model = sc.next();
					System.out.println("Enter the price : ");
					double price = sc.nextDouble();
					System.out.println("Enter the purchase date: ");
					String stringDate = sc.next();
					LocalDate releaseDate = LocalDate.parse(stringDate);
					System.out.println("Enter the colour (RED,BLUE,GREEN,YELLOW): ");
					Colour colour = Colour.valueOf(sc.next().toUpperCase());
					Car car = new Car(slNo, company, model, price, releaseDate, colour);
					carService.add(car);
				} catch (InputMismatchException e) {
					sc.nextLine();
					System.out.println(e);
				}
				break;
			case 2:
				carService.displayAll();
				break;
			case 3:
				System.out.println("Enter the search car company: ");
				String carName = sc.next();
				carService.search(carName);
				break;
			case 4:
				carService.sort();
				break;
			case 5:
				System.out.println("Enter the date in YYYY-mm-dd");
				String searchDate = sc.next();
				LocalDate date = LocalDate.parse(searchDate);
				System.out.println(carService.search(date));
				break;
			case 6:
				System.out.println("Deletion Operation");
				System.out.println("Enter the car name for deletion: ");
				String company=sc.next();
				carService.delete(company);
				break;
			case 7:
				try {
				System.out.println("Downloading CSV......");
				carService.downloadDetailsAsCSV();
				} catch (IOException e) {

					e.printStackTrace();
					}
				
				break;
			case 8:
				try {
					System.out.println("Downloading Json......");
				carService.downloadDetailsAsJson();
				} catch (IOException e) {

					e.printStackTrace();
					}
				break;
			case 9:
				System.exit(0);
				break;
			default:
				System.out.println("Invalid");
				break;
			}

		} while (true);

	}

}
