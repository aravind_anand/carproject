package main;

import java.util.Properties;

public abstract class CarServiceFactory {
	public static CarService getStudentservice(Properties properties)
	{
		CarService ob=null;
		String type=properties.getProperty("implementation");
		switch(type) {
		case "ArrayList":
			ob=new CarServiceArrayListImpl();
			break;
		case "Array":
			ob=new CarServiceArrayImpl();
			break;
		case "Database":
			String driver=properties.getProperty("driver");
			String url=properties.getProperty("url");
			String user=properties.getProperty("user");
			String password=properties.getProperty("password");
			ob=new CarServiceDatabaseImplementation(driver,url,user,password);
			break;
		default:
			System.out.println("Not valid");
		}
		
		return ob;
	}

}
