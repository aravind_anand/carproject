package main;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
//import java.util.Properties;
import java.util.StringJoiner;

public class CarServiceDatabaseImplementation implements CarService {

	String url, user, password, driver;

	public CarServiceDatabaseImplementation(String driver, String url, String user, String password) {

		this.driver = driver;
		this.url = url;
		this.user = user;
		this.password = password;

	}

	private Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName(driver);
		return DriverManager.getConnection(url, user, password);
	}

	private Car getCar(ResultSet resultSet) throws Exception {
		Car car = new Car();
		car.setSlNo(resultSet.getInt("slno"));
		car.setCompany(resultSet.getString("company"));
		car.setModel(resultSet.getString("model"));
		car.setPrice(resultSet.getDouble("price"));
		car.setReleaseDate(resultSet.getDate("release_date").toLocalDate());
		car.setColour(Colour.valueOf(resultSet.getString("color")));
		return car;
	}

	@Override
	public void add(Car car) {
		try {
			Connection connection = getConnection();
			// PreparedStatement statement=connection.prepareStatement("");
			String query = "INSERT INTO car (slno,company,model,price,release_date,color) VALUES (?,?,?,?,?,?)";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, car.getSlNo());
			preparedStatement.setString(2, car.getCompany());
			preparedStatement.setString(3, car.getModel());
			preparedStatement.setDouble(4, car.getPrice());
			preparedStatement.setDate(5, Date.valueOf(car.getReleaseDate()));
			preparedStatement.setString(6, car.getColour().toString());
			preparedStatement.executeUpdate();
			connection.close();

		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("Opened database successfully value inserted..!");
	}

	@Override
	public void displayAll() {
		try {
			Connection connection = getConnection();
			String query = "SELECT * FROM car;";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {

				Car car = getCar(resultSet);
				System.out.println(car);

			}
			resultSet.close();
			preparedStatement.close();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("Opened db value displayed");

	}

	@Override
	public void search(String carName) {
		try {
			Connection connection;
			connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM car where company=?");
			preparedStatement.setString(1, carName);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next())

			{

				Car car = getCar(resultSet);
				System.out.println(car);

			}
			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("Search  displayed");
	}

	@Override
	public void sort() {
		try {
			Connection connection;
			connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM car order by slno");
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next())

			{
				Car car = getCar(resultSet);
				System.out.println(car);

			}
			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("Sort  displayed");

	}

	@Override
	public List<Car> search(LocalDate releaseDate) {
		List<Car> carList = new ArrayList<Car>();
		try {
			Connection connection;
			connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM car where release_date=?");
			preparedStatement.setDate(1, Date.valueOf(releaseDate));
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {

				//Car car = getCar(resultSet);
				//System.out.println(car);	
				carList.add(getCar(resultSet));

			}
			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (carList);

	}

	@Override
	public void delete(String company) {

		try {
			Connection connection;
			connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM car where company=?");
			preparedStatement.setString(1, company);
			preparedStatement.executeUpdate();
			System.out.println("Element Deleted");
			preparedStatement.close();
			connection.close();

		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

	}

	@Override
	public void downloadDetailsAsCSV() throws IOException {
		try {
			Connection connection = getConnection();
			String query = "SELECT * FROM car;";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			ResultSet resultSet = preparedStatement.executeQuery();
			FileWriter fileWriter = new FileWriter("Download.csv");
			while (resultSet.next()) {
				fileWriter.write(getCSVString(getCar(resultSet)).toString());
			}
			fileWriter.close();
			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("CSV  displayed");

	}

	@Override
	public void downloadDetailsAsJson() throws IOException {
		try {
			Connection connection = getConnection();
			String query = "SELECT * FROM car;";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			ResultSet resultSet = preparedStatement.executeQuery();
			StringJoiner join = new StringJoiner(",", "[", "]");
			String data;
			FileWriter fileWriter = new FileWriter("DownloadJson.json");
			while (resultSet.next()) {
				data = getJsonString(getCar(resultSet));
				join.add(data);
			}
			fileWriter.write(join.toString());
			fileWriter.close();
			resultSet.close();
			preparedStatement.close();
			connection.close();

		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("Json  displayed");

	}

	

}
