package main;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class CarServiceArrayListImpl implements CarService {

	List<Car> carList = new ArrayList<>();
	
	//int count=0;

	@Override
	public void add(Car car) {
     if(carList.size()>0 && carList.contains(car))
     {
    	 System.out.println("Already exits");
     }		
     else
    	 {
    	 carList.add(car);
    	 }
    // count++;
		

	}

	@Override
	public void displayAll() {
		carList.forEach(System.out::println);
//		for(Car cars:carList)
//			System.out.println(cars);
	}

	@Override
	public void search(String carName) {
		
			//if (car.getCompany().equalsIgnoreCase(carName))

				//System.out.println(car);
		//for (Car car : carList) {
			System.out.println(carList.stream().filter(cname -> cname.getCompany().equals(carName))
					.collect(Collectors.toList()).toString());
	/*for (Car car : carList)
		System.out.println(car);*/
			
		}
	

	@Override
	public void sort() {
		Collections.sort(carList);
	}

	@Override
	public List<Car> search(LocalDate releaseDate) {
		return carList.stream().filter(date -> date.getReleaseDate().isAfter(releaseDate))
				.collect(Collectors.toList());
		//return carList;
	}

	@Override
	public void delete(String company) {
		
		for (Iterator<Car> it = carList.iterator(); it.hasNext();){
			Car car = it.next();
		    if (car.getCompany().equals(company)){
		        it.remove();
		    }
		}
		 System.out.println("Successfully deleted");
		/*
		Iterator<Car> itr = carList.iterator();
		 while (itr.hasNext()) {
			Car name1 = (Car) itr.next();
			for (int i = 0; i < carList.size(); i++) {
				if (name1.getCompany().equals(delete)) {
					itr.remove();
					System.out.println("Successfully deleted");
				}
			}
		}*/
	}

	@Override
	public void downloadDetailsAsCSV() throws IOException {

		FileWriter fileWriter = new FileWriter("Download.csv");
		for (Car car : carList)
			fileWriter.write(getCSVString(car));
		fileWriter.close();

	}

	@Override
	public void downloadDetailsAsJson() throws IOException {
		StringJoiner join = new StringJoiner(",", "[", "]");
		String data;
		FileWriter fileWriter = new FileWriter("downloadJson.json");
		for (Car car : carList) {

			data = getJsonString(car);
			join.add(data);
		}
		// System.out.println(join.toString());
		fileWriter.write(join.toString());

		fileWriter.close();
	}

}
